window.onload = () => {

    centresFetch();

}

reload = () => {

    let previousTable = document.getElementsByClassName("newRow");
    for (row = previousTable.length - 1; row > -1; row--) {
        previousTable[row].remove()   //AQUI ARA TENIM BUIDA LA TAULA
    }
    centresFetch();
}

crearCentre = () => {

    let centre = document.getElementById("nouCentreInput").value.trim();
    nouCentre = new FormData;
    nouCentre.append("nombre", centre);
    let params = {
        method: "POST",
        body: nouCentre,
        mode: "cors",
        cache: "no-cache"
    };
    fetch("http://alcyon-it.com/PQTM/pqtm_alta_colegios.php", params)
        .then((respuesta => {
            if (respuesta.ok) {
                return respuesta.text();
            }
            else { throw "Error en petición." }
        }))
        .then(consulta => {
            alert(consulta);
            reload();

        })
        .catch(e => { (console.log(e)) });


}

editarCentre = (idCentre) => {

    let name = document.getElementById(idCentre).value;
    let newName = name.trim();
    let escolaForm = new FormData;
    escolaForm.append("nombre", newName);
    escolaForm.append("idcolegio", idCentre);
    let params = {
        method: "POST",
        body: escolaForm,
        mode: "cors",
        cache: "no-cache"
    };
    fetch("http://alcyon-it.com/PQTM/pqtm_modificacion_colegios.php", params)
        .then((respuesta => {
            if (respuesta.ok) {
                return respuesta.text();
            }
            else { throw "Error en petición." }
        }))
        .then(consulta => {
            alert(consulta);

            reload();
        })
        .catch(e => { (console.log(e)) });

}

eliminarCentre = (idCentre) => {

    let escolaForm = new FormData;
    escolaForm.append("idcolegio", idCentre);
    let params = {
        method: "POST",
        body: escolaForm,
        mode: "cors",
        cache: "no-cache"
    };
    fetch("http://alcyon-it.com/PQTM/pqtm_baja_colegios.php", params)
        .then((respuesta => {
            if (respuesta.ok) {
                return respuesta.text();
            }
            else { throw "Error en petición." }
        }))
        .then(consulta => {
            alert(consulta);
            reload();

        })
        .catch(e => { (console.log(e)) });
}
centresFetch = () => {
    let params = {
        method: "POST",
        mode: "cors",
        cache: "no-cache"
    };
    fetch("http://alcyon-it.com/PQTM/pqtm_consulta_colegios.php", params)
        .then((respuesta => {
            if (respuesta.ok) {
                return respuesta.json();
            }
            else { throw "Error en petición." }
        }))
        .then(consulta => {
            const llistaCentres = document.getElementById('llistaCentres');
            for (let i in consulta) {
                const html =
                    `<tr class="newRow" >
                 <td>${consulta[i].idcolegio}</td>
                 <td><form>
                     <div class="inputWrapper">
                     <input id=${consulta[i].idcolegio}  value="${consulta[i].nombre}">
                     </div >
                     </form>
                </td>
               <td class="centeredButtons">
                   <button onclick="editarCentre(${consulta[i].idcolegio})"  type="button" class="activeButton">EDITAR</button>
                   <button type="button" onclick="eliminarCentre(${consulta[i].idcolegio})"class="activeButton">ELIMINAR</button>
                </td>
               </tr>`;
                llistaCentres.insertAdjacentHTML('beforeEnd', html);
            }
            sortTable(0);
        })
        .catch(e => { (console.log(e)) });
}

//POSICIONA CORRECTAMENT la fletxa
correctArrow = (n, dir) => {
    const arrow = "arr" + n;
    clearColumns = document.getElementsByClassName("arrow");

    for (let i in clearColumns) {
        clearColumns[i].innerHTML = " ";
    };
    currentColumn = document.getElementById(arrow);
    if (dir === "asc") {
        currentColumn.innerHTML = "&#x25BC";
    }
    if (dir === "desc") {
        currentColumn.innerHTML = "&#x25B2";
    }

}

//FUNCIÓ COPIADA DE W3SCHOOLS

sortTable = (n) => {

    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("llistaCentres");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";

    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows*/
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (n === 0) {
                    if (Number(x.innerHTML) > Number(y.innerHTML)) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                else {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            else if (dir == "desc") {
                if (n === 0) {
                    if (Number(x.innerHTML) < Number(y.innerHTML)) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                else {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
        correctArrow(n, dir)
    }
   
}
